package com.source.roma.util;

public class Message {
    private Integer code;
    private String description;
    private Object body;
    public enum TYPE {
        OK(200), // ok
        ERROR(400), // error
        AUTH_FAILED(401), // not auth set header with Authorization bearer token
        FORBIDDEN(403), // not auth as admin
        ;

        private int value = 0;

        private TYPE(int value) {
            this.value = value;
        }

        public static TYPE valueOf(int value) {    //手写的从int到enum的转换函数
            switch (value) {
                case 200:
                    return OK;
                case 400:
                    return ERROR;
                case 401:
                    return AUTH_FAILED;
                case 403:
                    return FORBIDDEN;
                default:
                    return null;
            }
        }

        public int value() {
            return this.value;
        }
    }

    public Message(TYPE code) {
        this.code = code.value();
        this.description = "";
        this.body = "";
    }
    public Message(TYPE code, Object body) {
        this.code = code.value();
        this.description = "";
        this.body = body;
    }

    public Message(TYPE code, String description, Object body) {
        this.code = code.value();
        this.description = description;
        this.body = body;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}
